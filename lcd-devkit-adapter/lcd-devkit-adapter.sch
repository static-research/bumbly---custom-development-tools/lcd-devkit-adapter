EESchema Schematic File Version 4
LIBS:lcd-devkit-adapter-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x30 J1
U 1 1 5D06FE6F
P 7150 3450
F 0 "J1" H 7230 3442 50  0000 L CNN
F 1 "LCD" H 7230 3351 50  0000 L CNN
F 2 "lcd-devkit-adapter:FPC_30" H 7150 3450 50  0001 C CNN
F 3 "~" H 7150 3450 50  0001 C CNN
	1    7150 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x22 J2
U 1 1 5D0759DA
P 5950 3950
F 0 "J2" H 6030 3942 50  0000 L CNN
F 1 "Hummingbird Pulse J24" H 6030 3851 50  0000 L CNN
F 2 "lcd-devkit-adapter:FPC_22" H 5950 3950 50  0001 C CNN
F 3 "~" H 5950 3950 50  0001 C CNN
	1    5950 3950
	1    0    0    -1  
$EndComp
NoConn ~ 6950 2250
NoConn ~ 6950 4950
NoConn ~ 6950 4850
NoConn ~ 6950 4650
Wire Wire Line
	6950 4450 6950 4350
Wire Wire Line
	6950 4350 6950 4250
Connection ~ 6950 4350
Wire Wire Line
	6950 4150 6950 4250
Connection ~ 6950 4250
Wire Wire Line
	6950 4150 6950 4050
Connection ~ 6950 4150
$Comp
L power:GND #PWR0101
U 1 1 5D082931
P 6800 4350
F 0 "#PWR0101" H 6800 4100 50  0001 C CNN
F 1 "GND" V 6805 4222 50  0000 R CNN
F 2 "" H 6800 4350 50  0001 C CNN
F 3 "" H 6800 4350 50  0001 C CNN
	1    6800 4350
	0    1    -1   0   
$EndComp
Wire Wire Line
	6950 4350 6800 4350
NoConn ~ 6950 3850
NoConn ~ 6950 3750
Wire Wire Line
	6950 3650 6800 3650
Wire Wire Line
	6800 3650 6800 4350
Connection ~ 6800 4350
Text GLabel 6950 3550 0    50   Input ~ 0
D0_N
Text GLabel 6950 3450 0    50   Input ~ 0
D0_P
Wire Wire Line
	6950 3350 6800 3350
Wire Wire Line
	6800 3350 6800 3650
Connection ~ 6800 3650
Text GLabel 6950 3250 0    50   Input ~ 0
CLK_N
Text GLabel 6950 3150 0    50   Input ~ 0
CLK_P
Wire Wire Line
	6950 3050 6800 3050
Wire Wire Line
	6800 3050 6800 3350
Connection ~ 6800 3350
Text GLabel 6950 2950 0    50   Input ~ 0
D1_N
Text GLabel 6950 2850 0    50   Input ~ 0
D1_P
Wire Wire Line
	6950 2750 6800 2750
Wire Wire Line
	6800 2750 6800 3050
Connection ~ 6800 3050
Wire Wire Line
	6950 2350 6800 2350
Wire Wire Line
	6800 2350 6800 2450
Connection ~ 6800 2750
Wire Wire Line
	6950 2450 6800 2450
Connection ~ 6800 2450
Wire Wire Line
	6800 2450 6800 2750
Text GLabel 6950 4550 0    50   Input ~ 0
LED_A
Text GLabel 6950 4750 0    50   Input ~ 0
LED_K
Text GLabel 6950 2550 0    50   Input ~ 0
TE
Text GLabel 6950 2650 0    50   Input ~ 0
RESET
$Comp
L power:+3.3V #PWR0102
U 1 1 5D0959EA
P 6700 2000
F 0 "#PWR0102" H 6700 1850 50  0001 C CNN
F 1 "+3.3V" H 6715 2173 50  0000 C CNN
F 2 "" H 6700 2000 50  0001 C CNN
F 3 "" H 6700 2000 50  0001 C CNN
	1    6700 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2050 6700 2050
Wire Wire Line
	6700 2050 6700 2000
Wire Wire Line
	6950 2150 6700 2150
Wire Wire Line
	6700 2150 6700 2050
Connection ~ 6700 2050
Wire Wire Line
	6950 3950 6700 3950
Wire Wire Line
	6700 3950 6700 2150
Connection ~ 6700 2150
$Comp
L power:+5V #PWR0103
U 1 1 5D09F0C9
P 5550 2850
F 0 "#PWR0103" H 5550 2700 50  0001 C CNN
F 1 "+5V" H 5565 3023 50  0000 C CNN
F 2 "" H 5550 2850 50  0001 C CNN
F 3 "" H 5550 2850 50  0001 C CNN
	1    5550 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2950 5550 2950
Wire Wire Line
	5550 2950 5550 2850
NoConn ~ 5750 3050
Wire Wire Line
	5750 3250 5550 3250
Wire Wire Line
	5550 3250 5550 2950
Connection ~ 5550 2950
NoConn ~ 5750 3450
Wire Wire Line
	5750 3550 5550 3550
Wire Wire Line
	5550 3550 5550 3250
Connection ~ 5550 3250
$Comp
L power:GND #PWR0104
U 1 1 5D0A2737
P 5400 5150
F 0 "#PWR0104" H 5400 4900 50  0001 C CNN
F 1 "GND" H 5405 4977 50  0000 C CNN
F 2 "" H 5400 5150 50  0001 C CNN
F 3 "" H 5400 5150 50  0001 C CNN
	1    5400 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 5050 5400 5050
Wire Wire Line
	5400 5050 5400 5150
Wire Wire Line
	5750 4750 5400 4750
Wire Wire Line
	5400 4750 5400 5050
Connection ~ 5400 5050
Wire Wire Line
	5750 4450 5400 4450
Wire Wire Line
	5400 4450 5400 4750
Connection ~ 5400 4750
Wire Wire Line
	5750 4150 5400 4150
Wire Wire Line
	5400 4150 5400 4450
Connection ~ 5400 4450
Wire Wire Line
	5750 3850 5400 3850
Wire Wire Line
	5400 3850 5400 4150
Connection ~ 5400 4150
Text GLabel 5750 4950 0    50   Input ~ 0
D0_P
Text GLabel 5750 4850 0    50   Input ~ 0
D0_N
Text GLabel 5750 4650 0    50   Input ~ 0
D1_N
Text GLabel 5750 4550 0    50   Input ~ 0
D1_P
Text GLabel 5750 4250 0    50   Input ~ 0
CLK_N
Text GLabel 5750 4350 0    50   Input ~ 0
CLK_P
NoConn ~ 5750 4050
NoConn ~ 5750 3950
NoConn ~ 5750 3750
NoConn ~ 5750 3650
NoConn ~ 5750 3350
Text Label 5750 3950 2    50   ~ 0
DSI2
Text Label 5750 3650 2    50   ~ 0
DSI3
NoConn ~ 5750 3150
$Comp
L power:+5V #PWR0105
U 1 1 5D0C1792
P 5900 2050
F 0 "#PWR0105" H 5900 1900 50  0001 C CNN
F 1 "+5V" H 5915 2223 50  0000 C CNN
F 2 "" H 5900 2050 50  0001 C CNN
F 3 "" H 5900 2050 50  0001 C CNN
F 4 "V" H 5900 2050 50  0001 C CNN "Spice_Primitive"
F 5 "dc 5" H 5900 2050 50  0001 C CNN "Spice_Model"
F 6 "Y" H 5900 2050 50  0001 C CNN "Spice_Netlist_Enabled"
	1    5900 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0106
U 1 1 5D0C1E17
P 6000 2350
F 0 "#PWR0106" H 6000 2200 50  0001 C CNN
F 1 "+3.3V" H 6015 2523 50  0000 C CNN
F 2 "" H 6000 2350 50  0001 C CNN
F 3 "" H 6000 2350 50  0001 C CNN
F 4 "V" H 6000 2350 50  0001 C CNN "Spice_Primitive"
F 5 "dc 3.3" H 6000 2350 50  0001 C CNN "Spice_Model"
F 6 "Y" H 6000 2350 50  0001 C CNN "Spice_Netlist_Enabled"
	1    6000 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5D0C4094
P 5850 2550
F 0 "#PWR0107" H 5850 2300 50  0001 C CNN
F 1 "GND" H 5855 2377 50  0000 C CNN
F 2 "" H 5850 2550 50  0001 C CNN
F 3 "" H 5850 2550 50  0001 C CNN
F 4 "V" H 5850 2550 50  0001 C CNN "Spice_Primitive"
F 5 "dc 0" H 5850 2550 50  0001 C CNN "Spice_Model"
F 6 "Y" H 5850 2550 50  0001 C CNN "Spice_Netlist_Enabled"
	1    5850 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 2550 5850 2450
Wire Wire Line
	5850 2450 5750 2450
Wire Wire Line
	5850 2450 5850 2250
Wire Wire Line
	5850 2250 5750 2250
Connection ~ 5850 2450
Wire Wire Line
	5750 2350 6000 2350
Wire Wire Line
	5750 2150 5900 2150
Wire Wire Line
	5900 2150 5900 2050
$Comp
L Connector:Conn_01x04_Male J4
U 1 1 5D0CEE97
P 4900 2400
F 0 "J4" H 5008 2681 50  0000 C CNN
F 1 "LCD_MISC" H 5008 2590 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 4900 2400 50  0001 C CNN
F 3 "~" H 4900 2400 50  0001 C CNN
	1    4900 2400
	1    0    0    -1  
$EndComp
Text GLabel 5100 2400 2    50   Input ~ 0
TE
Text GLabel 5100 2300 2    50   Input ~ 0
RESET
Text GLabel 5100 2500 2    50   Input ~ 0
LED_A
Text GLabel 5100 2600 2    50   Input ~ 0
LED_K
$Comp
L Connector:TestPoint TP0
U 1 1 5D0890A6
P 7850 2200
F 0 "TP0" H 7908 2318 50  0000 L CNN
F 1 "CLK_N" H 7908 2227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 8050 2200 50  0001 C CNN
F 3 "~" H 8050 2200 50  0001 C CNN
	1    7850 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5D08BE19
P 7850 2600
F 0 "TP1" H 7908 2718 50  0000 L CNN
F 1 "CLK_P" H 7908 2627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 8050 2600 50  0001 C CNN
F 3 "~" H 8050 2600 50  0001 C CNN
	1    7850 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5D08C730
P 7850 3000
F 0 "TP2" H 7908 3118 50  0000 L CNN
F 1 "D1_P" H 7908 3027 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 8050 3000 50  0001 C CNN
F 3 "~" H 8050 3000 50  0001 C CNN
	1    7850 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5D08C376
P 7850 3950
F 0 "TP4" H 7908 4068 50  0000 L CNN
F 1 "D0_N" H 7908 3977 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 8050 3950 50  0001 C CNN
F 3 "~" H 8050 3950 50  0001 C CNN
	1    7850 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J3
U 1 1 5D074FD4
P 5550 2250
F 0 "J3" H 5658 2531 50  0000 C CNN
F 1 "Power" H 5658 2440 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 5550 2250 50  0001 C CNN
F 3 "~" H 5550 2250 50  0001 C CNN
	1    5550 2250
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5D09101B
P 7850 3450
F 0 "TP3" H 7908 3568 50  0000 L CNN
F 1 "D1_N" H 7908 3477 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 8050 3450 50  0001 C CNN
F 3 "~" H 8050 3450 50  0001 C CNN
	1    7850 3450
	1    0    0    -1  
$EndComp
Text GLabel 7850 2200 0    50   Input ~ 0
CLK_N
Text GLabel 7850 2600 0    50   Input ~ 0
CLK_P
Text GLabel 7850 3000 0    50   Input ~ 0
D1_P
Text GLabel 7850 3450 0    50   Input ~ 0
D1_N
Text GLabel 7850 3950 0    50   Input ~ 0
D0_N
$Comp
L Connector:TestPoint TP5
U 1 1 5D0A45BA
P 7850 4300
F 0 "TP5" H 7908 4418 50  0000 L CNN
F 1 "D0_P" H 7908 4327 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 8050 4300 50  0001 C CNN
F 3 "~" H 8050 4300 50  0001 C CNN
	1    7850 4300
	1    0    0    -1  
$EndComp
Text GLabel 7850 4300 0    50   Input ~ 0
D0_P
$EndSCHEMATC
